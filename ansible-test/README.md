# ansible-test

This project will have ansible deployment scripts to get the IP and hostname of the target host.

# Usage 
ansible-playbook ansible-test.yaml -i <target_host_ip>, -e "ansible_ssh_user=<user_name>" --private-key <ssh_key>

Example: 
  ansible-playbook ansible-test.yaml -i 192.168.122.246, -e "ansible_ssh_user=ubuntu" --private-key ~/.ssh/id_rsa
