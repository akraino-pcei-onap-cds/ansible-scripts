# This file references Cisco Ios Ansible
# modules from https://docs.ansible.com/ansible/latest/collections/cisco/ios/index.html
 
## Step : 0 Pull the CDS docker image

docker pull amcop/ccsdk-py-executor-amcop-multi-cloud-saas:1.2.1-SNAPSHOT-latest
 
## Step : 1 You should copy this project folder /cisco-ansible/

sudo mkdir -p /opt/ansible-testing/
sudo cp -r ~/cisco-ansible /opt/ansible-testing/cisco-ansible
sudo chmod -R 777 /opt/ansible-testing/
 
## Step : 2 Execute the below command to start the docker container

docker run -it --entrypoint /bin/bash -v "/opt/ansible-testing/":/ansible-testing amcop/ccsdk-py-executor-amcop-multi-cloud-saas:1.2.1-SNAPSHOT-latest
 
## Step : 4 Execute the following commands before we execute the ansible scripts.

ansible-galaxy collection install cisco.ios
pip install ansible-pylibssh
export ANSIBLE_HOST_KEY_CHECKING=False
ansible-galaxy collection list

 
## Step : 4 Execute the below command to execute the ansible command within the docker container

cd /ansible-testing/cisco-ansible/
ansible-playbook -vv -i inventory.ini main.yaml
