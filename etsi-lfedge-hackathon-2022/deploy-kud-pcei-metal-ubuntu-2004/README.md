# This project contains ansible scripts to install Kubeadm on target Equinix metal server with  Ubuntu 20.04 OS installed.

## Ansible command usage
ansible-playbook -vv main.yaml -i <target_IP>, -e "target_ip=<target_IP>" -e "target_host_user=<target_user_name>" --private-key <target_private_key> --ssh-common-args='-o StrictHostKeyChecking=no'
