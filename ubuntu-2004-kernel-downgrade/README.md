# Downgrade ubuntu kernal version

This project will downgrade the kernal version on the target host

# Usage 
ansible-playbook -vv main.yaml -i <target_IP>, -e "target_ip=<target_IP>" -e "target_host_user=<target_user_name>" --private-key <target_private_key> --ssh-common-args='-o StrictHostKeyChecking=no' 

# Bring up kubeadm on the target host
ansible-playbook -vv main.yaml -i 192.168.122.99, -e "target_ip=192.168.122.99" -e "target_host_user=ubuntu" --private-key /home/opnfv/.ssh/id_rsa --ssh-common-args='-o StrictHostKeyChecking=no'



